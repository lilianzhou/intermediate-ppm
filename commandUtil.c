//commandUtil.c, midterm project
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#include <stdio.h>
#include <string.h>

#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"
#include "imagememory.h"

//open_file function that reads the command-line arguments 
int open_file(int argc, char *argv[]) {
  // check the input and output filename are supplied
  if (argc < 3) {
    fprintf(stderr, "1: Failed to supply input filename or output filename, or both.\n");
    return 1;
  }
  // open the input file
  FILE *input_name = fopen(argv[1], "rb");
  if (input_name == NULL) {
    fprintf(stderr, "2: Specified input file could not be found.\n");
    return 2;
  }
  Image* im = readPPM(input_name);
  //if reading input file fails, the readPPM returns a NULL Image pointer
  if (im == NULL) {
    imagefree(im);
    return 3;
  }
  // check the output file opening
  FILE *output_name = fopen(argv[2],"wb");
  if (output_name == NULL) {
    fprintf(stderr, "4: Specified output file could not be found.\n");
    return 4;
  }
  fclose(output_name);

  if(argc < 4) {
    fprintf(stderr, "5: No operation specified. \n");
    return 5;
  }
  int err_type = choice(im,argc,argv);
  imagefree(im);
  fclose(input_name);
  return err_type;
}//end of readPPM function

int choice(Image* im, int argc, char *argv[]) {
  // for the invert operation
  if (strcmp(argv[3], "invert") == 0) {
    // User has to define the target color for red, green and blue, and the tolerance
    if (argc != 8) {
      fprintf(stderr, "6: Incorrect number of arguments specified for the invert operation.\n");
      return 6;
    }
    // Collect the target color for red, target color for green, target color for blue and the tolerance
    int target_red, target_green, target_blue, tolerance;
    char *arg4 = argv[4];
    char *arg5 = argv[5];
    char *arg6 = argv[6];
    char *arg7 = argv[7];
    // check target_red, target_green, target_blue, tolerance are integers
    if (sscanf(arg4, " %d", &target_red) != 1) {
      fprintf(stderr, "6: Incorrect target red color specified for the invert operation.\n");
      return 6;
    }   
    if (sscanf(arg5, " %d", &target_green)!= 1) {
      fprintf(stderr, "6: Incorrect target green color specified for the invert operation.\n");
      return 6;
    }
    if (sscanf(arg6, " %d", &target_blue) != 1) {
      fprintf(stderr, "6: Incorrect target blue color specified for the invert operation.\n");
      return 6;
    }
    if (sscanf(arg7, " %d", &tolerance) != 1) {
      fprintf(stderr, "6: Incorrect tolerance for the invert operation\n");
      return 6;
    }
    // check for the input data type (should be int, no double allowed)
    double try_red, try_green, try_blue, try_tol;
    sscanf(arg4, " %lf", &try_red);
    sscanf(arg5, " %lf", &try_green);
    sscanf(arg6, " %lf", &try_blue);
    sscanf(arg7, " %lf", &try_tol);    
    if (try_red != (double)target_red) {
      fprintf(stderr, "6: Incorrect target red color data type for the invert operation.\n");
      return 6;
    }
    if (try_green != (double)target_green) {
      fprintf(stderr, "6: Incorrect target green color data type for the invert operation.\n");
      return 6;
    }
    if (try_blue != (double)target_blue) {
      fprintf(stderr, "6: Incorrect target blue color data type for the invert operation.\n");
      return 6;
    }
    if (try_tol != (double)tolerance) { 
      fprintf(stderr, "6: Incorrect tolerance data type for the invert operation.\n"); 
      return 6;
    }
    // for test:  printf("%d %d %d %d \n",target_red, target_green, target_blue, tolerance);
    // check the range of tr, tg, tb, tolerance
    if (!(tolerance >= 0 &&  tolerance <= 255)) {
      fprintf(stderr, "6: Incorrect tolerance range given for the invert operation.\n");
      return 6;
    }
    if (target_red < 0 || target_red > 255) {
      fprintf(stderr, "6: Incorrect target red color range given for the invert operation.\n");
      return 6;
    }
    if (target_green < 0 || target_green > 255) {
      fprintf(stderr, "6: Incorrect target green color range given for the invert operation.\n");
      return 6;
    }
    if (target_blue < 0 || target_blue > 255) {
      fprintf(stderr, "6: Incorrect target blue color range given for the invert operation.\n");
      return 6;
    }
    im = invert(im, target_red, target_green, target_blue, tolerance);
  }

  // For the reflect details
  else if (strcmp(argv[3], "reflect") == 0) {
    if (argc != 4) {
      fprintf(stderr, "6: Incorrect number of arguments specified for the reflect operation.\n");
      return 6;
    }
    im = reflect(im);
  }
  
  // For the crop operation details
  else if (strcmp(argv[3], "crop") == 0) {
    // User has to define top-left and bottom-right corner coordinates
    if (argc != 8) {
      fprintf(stderr, "6: Incorrect number of arguments specified for the crop operation.\n");
      return 6;
    }
    // Collect the coordinates of the top-left and bottom-right corners
    int new_top_x, new_top_y, new_bottom_x, new_bottom_y;
    char *arg4 = argv[4];
    char *arg5 = argv[5];
    char *arg6 = argv[6];
    char *arg7 = argv[7];
    // coordinates has to be integer
    if (sscanf(arg4, " %d",&new_top_x) != 1) {
      fprintf(stderr, "6: Incorrect crop coordinates specified for the crop operation.\n");
      return 6;
    }
    if (sscanf(arg5, " %d",&new_top_y)!= 1) {
      fprintf(stderr, "6: Incorrect crop coordinates specified for the crop operation.\n");
      return 6;
    }
    if (sscanf(arg6, " %d",&new_bottom_x) != 1) {
      fprintf(stderr, "6: Incorrect crop coordinates specified for the crop opertaion.\n");
      return 6;
    }
    if (sscanf(arg7, " %d",&new_bottom_y) != 1) {
      fprintf(stderr, "6: Incorrect crop coordinates specified for the crop operation.\n");
      return 6;
    }
    // check for integer input type
    double try_top_x, try_top_y, try_bot_x, try_bot_y;
    sscanf(arg4, " %lf", &try_top_x);
    sscanf(arg5, " %lf", &try_top_y);
    sscanf(arg6, " %lf", &try_bot_x);
    sscanf(arg7, " %lf", &try_bot_y);
    if (try_top_x != (double)new_top_x) {
      fprintf(stderr, "6: Incorrect input coordinate data type for the crop operation.\n")\
;
      return 6;
    }
    if (try_top_y != (double)new_top_y) {
      fprintf(stderr, "6: Incorrect input coordinate data type for the crop operation.\n")\
;
      return 6;
    }
    if (try_bot_x != (double)new_bottom_x) {
      fprintf(stderr, "6: Incorrect input coordinate data type for the crop operation.\n")\
;
      return 6;
    }
    if (try_bot_y != (double)new_bottom_y) {
      fprintf(stderr, "6: Incorrect input coordinate data type for the crop operation.\n");
      return 6;
    }
    // the coordinates for top-left and bottom-right has to be constaint
    if (new_bottom_x > im->rows || new_bottom_y > im->cols || new_bottom_x <= 0 || new_bottom_y <= 0) {
      fprintf(stderr, "7: Incorrect crop coordinates(right bottom corner) specified for the crop operation.\n");
      return 7;
    }
    if (new_top_x < 0 || new_top_y < 0 || new_top_x >= new_bottom_x || new_top_y >= new_bottom_y) {
      fprintf(stderr, "7: Incorrect crop coordinates(left top corner) specified for the crop operation.\n");
      return 7;
    }
    im = crop(im, new_top_x, new_top_y, new_bottom_x, new_bottom_y);
  }

  // For the grayscale details
  else if (strcmp(argv[3], "grayscale") == 0) {
    if (argc != 4) {
      fprintf(stderr, "6: Incorrect number of arguments specified for the grayscale operation.\n");
      return 6;
    }
    im = grayscale(im);
  }

  // For the contrast details
  else if (strcmp(argv[3], "contrast") == 0) {
    // Check the user inputs the right amount of arguments for contrast 
    if (argc != 5) {
      fprintf(stderr, "6: Incorrect number of arguments specified for the contrast operation.\n");
      return 6;
    }
    double factor;
    char *arg4 = argv[4];
    if (sscanf(arg4, " %lf",&factor) != 1) {
      fprintf(stderr, "6: Incorrect factor input for the contrast operation.\n");
      return 6;
    }
    // checks for error, factor has to be greater than 0
    if (factor <= 0) {
      fprintf(stderr, "6: Incorrect factor value for the contrast operation.\n");
      return 6;
    }
    im = contrast(im, factor); 
  }
  // no operation name specified or invalid
  // else if (argc < 4 || (!(strcmp(argv[3], "contrast") == 0 || strcmp(argv[3], "crop") == 0 || strcmp(argv[3], "invert") == 0 || strcmp(argv[3], "reflect") == 0 || strcmp(argv[3], "grayscale") == 0))) {
  else {
    fprintf(stderr, "5: Operation specified was invalid.\n");
  }
  
  return output_file(im, argv);
}//end of choice function

int output_file(Image *outputImage, char *argv[]) {
  FILE *output_name = fopen(argv[2], "wb");
  if (output_name == NULL) {
    fprintf(stderr, "4: Specified output file could not be opened for writing.\n");
    return 4;
  }
  int error_write = writePPM(output_name,outputImage);
  if (error_write == -1) {
    fprintf(stderr, "4: Writing output somehow fails.\n");
    return 4;
  }
  fclose(output_name);
  return 0;
} //end of output_file function

