//imageManip.c, functions that do specified operations
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"
#include "imagememory.h"


//crop:
/*
 * take in the image and top left corner and lower right corner
 * and return the pointer of the new image 
 */
Image* crop(Image* im, int ly, int lx, int ry, int rx) {	
  Image * newImage=imagealloc(rx-lx,ry-ly);
  for (int i=0;i<rx-lx;i++) {
      for (int j=0;j<ry-ly;j++) {
	newImage->data[newImage->cols*i+j] = im->data[im->cols*(lx+i)+ly+j];
      }
  }
  imagefree(im);
  return newImage;
}


//invert:
/*
 * take in the image, target red, target greem, target blue, tolerance
 * and return the pointer of the new image
 */
Image* invert(Image*im, int tr, int tg, int tb, int t) {
  Image * newImage=imagealloc(im->rows,im->cols);
  for (int i=0;i < im->rows;i++) {
    for (int j=0; j < im->cols;j++) {
      if(abs((int)im->data[im->cols*i+j].r - tr) <= t) {
	newImage->data[im->cols*i+j].r = ~im->data[im->cols*i+j].r;
      }
      else if (abs((int)im->data[im->cols*i+j].r - tr)>t) {
	newImage->data[im->cols*i+j].r = im->data[im->cols*i+j].r;
      }
      if(abs((int)im->data[im->cols*i+j].g - tg)<=t) {
	newImage->data[im->cols*i+j].g = ~im->data[im->cols*i+j].g;
      }
      else if (abs((int)im->data[im->cols*i+j].g - tg)>t) {
	newImage->data[im->cols*i+j].g = im->data[im->cols*i+j].g;
      }
      if(abs((int)im->data[im->cols*i+j].b - tb)<=t) {
	newImage->data[im->cols*i+j].b = ~im->data[im->cols*i+j].b;
      }
      else if (abs((int)im->data[im->cols*i+j].b - tb)>t) {
	newImage->data[im->cols*i+j].b = im->data[im->cols*i+j].b;
      }
    }
  }
  //calls the freeimage function to free the original image pointer
  imagefree(im);
  //returns the new image pointer to the calling function
  return newImage;
}


//reflect:
/*
 * take in the image 
 * and return the pointer of the new image
 */
Image* reflect(Image*im) {
  Image * newImage=imagealloc(im->rows,im->cols);
  for (int i=0;i < im->rows;i++) {
    for (int j=0; j < im->cols;j++) {
      newImage->data[im->cols*i+j]=im->data[im->cols*(i+1)-j-1];
    }
  }
  //calls the imagefree function to free the original image pointer
  imagefree(im);
  //returns the new image pointer to the calling function
  return newImage;
}

//grayscale:
/*
 * take in the image 
 * and return the pointer of the new image
 */

Image *grayscale(Image *im) {
  Image *newImage = imagealloc(im->rows, im->cols);
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      double intensity_current = 0.30*im->data[im->cols*i+j].r + 0.59*im->data[im->cols*i+j].g + 0.11*im->data[im->cols*i+j].b;
      newImage->data[newImage->cols*i+j].r = (unsigned char)intensity_current;
      newImage->data[newImage->cols*i+j].g = (unsigned char)intensity_current;
      newImage->data[newImage->cols*i+j].b = (unsigned char)intensity_current;
    }
  }
  //calls the imagefree function to free the original image pointer
  imagefree(im);
  return newImage;
}

//contrast:
/*
 * take in the image, range, factor
 * and return the pointer of the new image
 */

Image *contrast(Image* im, double fac) {
  Image *newImage = imagealloc(im->rows,im->cols);
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      newImage->data[im->cols*i+j].r = modcontrast(im->data[im->cols*i+j].r, fac);
      newImage->data[im->cols*i+j].g = modcontrast(im->data[im->cols*i+j].g, fac);
      newImage->data[im->cols*i+j].b = modcontrast(im->data[im->cols*i+j].b, fac);
    }
  }
  //calls the imagefree function to free the original pointer
  imagefree(im);
  //returns the new image pointer to the calling function
  return newImage;
}

//modcontrast:
/*
 * helper function for contrast function that converts value to be in the range
 * returns result of the conversion
 */

unsigned char modcontrast(unsigned char orig_color, double fac) {
  // set the range to be [-0.5,0.5]
  double range = 0.5;
  double scale = (double)(range - (-range))/(255-0);
  double converted = -range + ((int)orig_color - 0)* scale;
  double result_double = converted * fac;
  int result = 0 + (result_double - (-range)) / scale;
  
  if (result > 255) {
    result = (unsigned char)255;
  } else if (result < 0) {
    result = (unsigned char)0;
  } else {
    result = (unsigned char)result;
  }
  //returns result to calling function
  return result;
}
