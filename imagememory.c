//imagememory.c for allocation/deallocating memory
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#include <stdio.h>
#include <stdlib.h>
#include "imagememory.h"
#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h"

/*
 * imagealloc function for allocating the memory to store the images by
 * allocating structure Image and structure pixels
 */

Image* imagealloc(int x, int y) {
  Image* result;
  result=(Image*)malloc(sizeof(Image));//memory to hold structure Image
  if (result==NULL) {//check the memory
    return NULL;
  }
  result->data=(Pixel*)malloc(sizeof(Pixel)*x*y);//memory to hold all the pixels
  if (result->data==NULL) {
    free(result);//free the previous memory for holding the result
    return NULL;
  }
  result->cols=y;//reassign the new cols and rows for the new image.
  result->rows=x;
  return result;//return the new image location
}


/* 
 * the imagefree function helps to free the dynamically allocated memory
 */
void imagefree(Image* p) {
  if (p==NULL) {
    return;
  }
  free(p->data);//free the pixels first then free the memory for holding structure Image
  free(p);
}
