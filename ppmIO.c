//ppmIO.c
//601.220, Summer 2018
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#include "ppmIO.h"
#include "commandUtil.h"
#include "imageManip.h" 

/* read PPM formatted image from a file (assumes fp != NULL) */
Image* readPPM(FILE *fp) {

  /* file is assumed to be open, checked by commandUtil.c*/
  if (!fp) {
    return NULL;
  }

  /* error and abort if the tag of PPM is not P6 */
  char tag[3];
  fscanf(fp, "%2s", tag);
  tag[2] = 0;
  if (strcmp(tag, "P6") != 0) {
    fprintf(stderr, "3: Specified input file is not properly-formatted PPM file: no P6 tag.\n");
    return NULL;
  }

  /* read in number of columns, rows and colors (ignore comment line) */
  int num_col = 0; //width
  int num_row = 0; //height
  int color = 0;
  fpos_t position;
  char ch = ' ';
  while (isspace(ch)) {
    fgetpos(fp,&position);
    ch = fgetc(fp);
  }
  //for test: printf("%c\n",ch);
  if (ch == '#') {
    while (ch != '\n') {
      ch = fgetc(fp);
    }
    //for test: fgetpos(fp, &position);
  } else {
    fsetpos(fp, &position);
  }
  //for test: printf("%c\n",ch);
    fscanf(fp, " %d", &num_col);
    fscanf(fp, " %d", &num_row);
    fscanf(fp, " %d", &color);
  //for test:  printf("num_col %d, num_row %d, color %d\n",num_col,num_row,color);

  /* error checking */
  if (color != 255) {
    fprintf(stderr, "3: Specified input file is not properly-formatted PPM file: color is not 255.\n");
    return NULL;
  }
  if (num_col == 0 || num_row == 0 || color == 0) {
    fprintf(stderr, "3: Specified input file is not properly-formatted PPM file: read in columns/rows/color error (missing information or wrong format).\n");
    return NULL;
  }
  /* check the following whitespace character */
  char next = fgetc(fp);
  if (!isspace(next)) {
    fprintf(stderr, "3: Specified input file is not properly-formatted PPM file: no whitespace following with size specification.\n");
    return NULL;
  }

  /* allocated memory for pixel data */
  Image *im = malloc(sizeof(Image));
  if (im == NULL) {
    fprintf(stderr,"3:reading input fails, allocating memory fails for the input file.\n");
    return NULL;
  }
  Pixel *array = malloc(sizeof(Pixel)* num_col* num_row);
  if (array == NULL) {
    fprintf(stderr, "3: reading input fails, allocating memory fails for the input file.\n");
    free(im);
    return NULL;
  }
  for (int i = 0; i < num_row; i++) {
    for (int j = 0; j <num_col; j++) {
      Pixel current;
      int readin;
      readin = fread(&current, sizeof(Pixel),1,fp);
      array[i*num_col + j].r = current.r;
      array[i*num_col + j].g = current.g;
      array[i*num_col + j].b = current.b;
      /* check if the number of elements are correct */
      if (readin != 1) {
	fprintf(stderr, "3: Specified input file is not properly-formatted PPM file: lacking in pixel data.\n");
	return NULL;
      }
    }
  }
  im->data = array;
  im->rows = num_row;
  im->cols = num_col;
  
  /* success, so return the information being read */
  return im;
}




/* write PPM formatted image to a file (assumes fp != NULL and img != NULL) */
int writePPM(FILE *fp, const Image* im) {

  /* abort if either file pointer is dead or image pointer is null; indicate failure with -1 */
  if (!fp || !im) {
    return -1;
  }
  /* write tag and dimensions; colors is always 255 */
  fprintf(fp, "P6\n%d %d\n%d\n", im->cols, im->rows, 255);
  /* write pixels */
  int num_pixels_written = (int) fwrite(im->data, sizeof(Pixel), (im->rows) * (im->cols), fp);
  /* check if write was successful or not; indicate failure with -1 */
  if (num_pixels_written != (im->rows) * (im->cols)) {
    return -1;
  }
  /* success, so return number of pixels written */
  return num_pixels_written;
}



