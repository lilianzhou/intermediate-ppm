# Makefile

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g

# Links together files needed to create executable
project: project.o ppmIO.o commandUtil.o imageManip.o imagememory.o
	$(CC) -o project project.o ppmIO.o commandUtil.o imageManip.o imagememory.o

# Compiles project.c to create project.o
project.o: project.c ppmIO.h commandUtil.h imageManip.h imagememory.h
	$(CC) $(CFLAGS) -c project.c

# Compiles ppmIO.c to create ppmIO.c
ppmIO.o: ppmIO.c ppmIO.h
	$(CC) $(CFLAGS) -c ppmIO.c

#Compiles commandUtil.c to create commandUtil.o
commandUtil.o: commandUtil.c commandUtil.h
	$(CC) $(CFLAGS) -c commandUtil.c

#Compiles imageManip.c to create imageManip.o
imageManip.o: imageManip.c imageManip.h
	$(CC) $(CFLAGS) -c imageManip.c

#Compiles imageManip.c to create imageManip.o
imagememory.o: imagememory.c imagememory.h
	$(CC) $(CFLAGS) -c imagememory.c



# Removes all object files and the executable named main,
# so we can start fresh
clean:
	rm -f *.o project
