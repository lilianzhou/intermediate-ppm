#ifndef debug_h
#define debug_h

#ifdef DEBUG
// from https://stackoverflow.com/questions/1644868/c-define-macro-for-debug-printing and https://stackoverflow.com/questions/46268020/c-variadic-macro-do-not-compile
#define debug_print(fmt, ...) do { fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__); 	} while (0)
#include <stdio.h>
#else
#define debug_print(fmt, ...) do { if(0) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, __LINE__, __func__, ## __VA_ARGS__); 	} while (0)
#endif

#endif
