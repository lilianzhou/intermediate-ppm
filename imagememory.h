//imagememory.h, header file for imagememory.c
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#ifndef imagememory_H
#define imagememory_H
#include "ppmIO.h"

/*
 * allocate the memory for the size of x*y image
 *
 *
 *
 */
Image* imagealloc(int x, int y);
/*
 * free the memory allocated by imagealloc
 *
 *
 */
void imagefree(Image* p);
//




#endif
