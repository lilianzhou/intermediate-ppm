#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageFun.h"
#include "imagememory.h"

Image *grayscale(Image *im) {
  Image *newImage = imagealloc(im->cols, im->rows);
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      int intensity_current = 0.30*(int)im->data[im->cols*i+j].r + 0.50*(int)im->data[im->cols*i+j].g + 0.11*(int)im->data[im->cols*i+j].b;
      newImage->data[newImage->cols*i+j].r = (unsigned char)intensity;
      newImage->data[newImage->cols*i+j].g = (unsigned char)intensity;
      newImage->data[newImage->cols*i+j].b = (unsigned char)intensity;
    }
  }
  imagefree(im);
  return newImage;
}

Image *contrast(Image* im, double range, double fac) {
  Image *newImage = imagealloc(im->cols,im->rows);
  for (int i = 0; i < im->rows; i++) {
    for (int j = 0; j < im->cols; j++) {
      newImage->data[im->cols*i+j].r = modcontrast(im->data[im->cols*i+j].r, range, fac);
      newImage->data[im->cols*i+j].g = modcontrast(im->data[im->cols*i+j].g, range, fac);
      newImage->data[im->cols*i+j].b = modcontrast(im->data[im->cols*i+j].b, range, fac);
    }
  }
  imagefree(im);
  return newImage;
}

unsigned char modcontrast(unsigned char orig_color, double range, double fac) {
  double scale = (double)(range - (-range))/(255-0);
  double converted = -range + ((int)orig_color - 0)* scale;
  unsigned char result = (unsigned char) converted * fac;
  if (result > 255) {
    result = (unsigned char)255;
  } else if (result < 0) {
    result = (unsigned char)0;
  }
}

