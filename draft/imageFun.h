#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ppmIO.h"
#include "imagememory.h"

Image *grayscale(Image *im);

Image *contrast(Image *im, double range, double fac);

unsigned char modcontrast(unsigned char orig_color, double range, double fac);
