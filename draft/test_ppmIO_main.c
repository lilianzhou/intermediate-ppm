#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include "ppmIO.h"

int main() {
  FILE *fp = fopen("nika.ppm","rb");
  Image *original_image = readPPM(fp);
  FILE *output = fopen("output.ppm","wb");
  writePPM(output,original_image);
  return 0;
}
