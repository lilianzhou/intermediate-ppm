//commandUtil.h
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#ifndef commandUtil_H
#define commandUtil_H

#include "ppmIO.h"
#include "imageManip.h"

//open_file function to open the file inputted by user and pass to readPPM function
int open_file(int argc, char *argv[]);

//choice function to determine operation inputted by user and call correct operation function
int choice(Image* im, int argc, char *argv[]);

//output_file function to determine specified output file name and open to pass to writePPM function
int output_file(Image *outputImage, char *argv[]);

#endif
