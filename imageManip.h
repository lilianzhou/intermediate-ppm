//imageManip.h, header file for imageManip.c
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#ifndef imageManip_H
#define imageManip_H
#include "ppmIO.h"
#include "commandUtil.h"
#include "imagememory.h"

//crop function
Image* crop(Image *im, int ly, int lx, int ry, int rx);

//invert function
Image* invert(Image* im, int tr, int tg, int tb, int t);

//reflect function
Image* reflect(Image* im);

//grayscale function
Image *grayscale(Image *im);

//contrast function
Image *contrast(Image *im, double fac);

//modcontrast function, helper function for contrast function
unsigned char modcontrast(unsigned char orig_color, double fac);

#endif
