//project.c, contains main function 
//Yuqing Eva Pan, Weiyu Peng, Lilian Zhou
//ypan23, wpeng9, lzhou44

#include <stdio.h>
#include <stdlib.h>
#include "ppmIO.h"
#include "imageManip.h"
#include "commandUtil.h"
#include "imagememory.h"

//main function
int main(int argc, char *argv[]) {
  //call open_file function and pass to it the command line arguments
  int error_type = open_file(argc, argv);
  return error_type;
}//end of program
